// Load modules from npm
var gulp        = require('gulp');
var $           = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload      = browserSync.reload;

// Config
var AUTOPREFIXER_BROWSERS = [
  'last 2 versions'
];
var HTML_DIR = 'app/';
var SCSS_DIR = 'app/styles/scss/';
var CSS_DIR  = 'app/styles/';

// Compile SASS with libSass
// Add vendor prefixes with Autoprefixr
// Create inline sourcemaps for css/scss
gulp.task('styles', function() {
  return gulp.src(SCSS_DIR + 'main.scss')
    .pipe($.changed(CSS_DIR))
    .pipe($.sourcemaps.init())
      .pipe($.sass({
        onError: console.error.bind(console, 'SASS error:'),
        cascade: false,
        outputStyle: 'nested',
        precision: 8
      }))
    .pipe($.autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(CSS_DIR))
    .pipe($.size({title: 'styles'}));
});

// Serve files through browserSync and watch files to reload on change
// Use proxy if need more than a static server
gulp.task('serve', ['styles'], function() {
  browserSync({
    //proxy: 'localhost',
    server: ['app'],
    notify: false
  });

  gulp.watch(SCSS_DIR + '**/*.scss', ['styles', reload]);
  gulp.watch(HTML_DIR + '*.html', reload);
});