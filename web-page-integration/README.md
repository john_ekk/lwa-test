# Web page integration

## Question

You have to develop as much as possible of the Web page described in the attached images.

The webpage needs to maintain correct proportions when being resized (being responsive), so we recommend that you use bootstrap software.

## Usage

This project requires [Node.js](http://nodejs.org/) and [npm](https://www.npmjs.com/).

Run `npm install` and then `bower install` to install the project dependencies.

Once done, type `gulp serve` to launch the server. Check the console output to access the website and `gulpfile.js` for details on the Gulp configuration.
